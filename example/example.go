package main

import (
	"fmt"
	"gitee.com/jiangjiali/hcl"
	"os"
	"strconv"
)

var server struct {
	LogLevel          string `hcl:"log-level"`
	LogPath           string `hcl:"log-path"`
	PoolSize          int    `hcl:"pool-size"`
	WSAddr            string `hcl:"ws-addr"`
	CertFile          string `hcl:"cert-file"`
	KeyFile           string `hcl:"key-file"`
	TCPAddr           string `hcl:"tcp-addr"`
	MaxConnNum        int    `hcl:"max-conn-num"`
	HeartbeatInterval int    `hcl:"heartbeat-interval"`
	MongoAddr         string `hcl:"mongo-addr"`
	MongoPool         uint64 `hcl:"mongo-pools"`
}

func main() {
	d, err := os.ReadFile("config.hcl")
	if err != nil {
		fmt.Printf("read err: %s\n", err)
	}

	err = hcl.Unmarshal(d, &server)
	if err != nil {
		fmt.Printf("json.err: %s\n", err)
	}
	fmt.Printf(strconv.FormatUint(server.MongoPool, 10))
}
